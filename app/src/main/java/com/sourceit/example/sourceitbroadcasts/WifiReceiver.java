package com.sourceit.example.sourceitbroadcasts;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class WifiReceiver extends BroadcastReceiver {

    public static final int ID = 101;
    public static final int NOTIFY_ID = 1101;

    @Override
    public void onReceive(Context context, Intent intent) {

//        String state = isWifiEnabled(context) ? "wifi enable" : "wifi disable";
        String state = isAirplaneModeOn(context) ? "AirplaneMode enable" : "AirplaneMode disable";
        Log.d("wifi", "state");
        Toast.makeText(context, state, Toast.LENGTH_SHORT).show();


        //create intent (click notification)
        Intent notificationIntent = new Intent(context, MainActivity.class);
        notificationIntent.addFlags(FLAG_ACTIVITY_NEW_TASK);

        PendingIntent contentIntent = PendingIntent.getActivity(context,
                ID, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationManager nm = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);


        Notification.Builder builder = new Notification.Builder(context);
        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setContentTitle("Mode")
                .setContentText(state);

        Notification n = builder.build();
        nm.notify(NOTIFY_ID, n);

    }

    private static boolean isAirplaneModeOn(Context context) {
        return Settings.Global.getInt(context.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) != 0;

    }

    private boolean isWifiEnabled(Context context) {
        ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
    }

    private boolean isWifiEnabled(Intent intent) {
        NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
        return networkInfo != null && networkInfo.isConnected();
    }





}
